# M17UF1R1 - GAMEPLAY COMPLERT

## Resum

El nostre personatge ha d'esquivar el màxim de boles de foc 🔥 que cauen del cel i esclaten a terra.
Si una bola la toca, ella mort i acaba la partida. Si el personatge cau per un dels extrems del terra, acaba la partida.
El joc té un comptador de temps enrere.
Si el rellotge arriba a 0, s'acaba el joc. La condició de victòria es donarà segons el mode de partida escollit.

## Punts a complir del repte

### Mode de joc 1

El jugador mou al personatge.
El joc generarà aleatòriament boles de foc que cauran del cel a terra.

- Condicions de derrota:
- Si el personatge cau per un dels extrems del terra, perd.
- Si el personatge és colpejat per una bola de foc.
- Condicions de victòria:
- Si el jugador no és colpejat per cap bola de foc quan acaba el temps determinat.

### Mode de joc 2

El jugador controla on es generen les boles de foc.
El personatge correrà d'un extrem a un altre i haurà de saber quan canviar de direcció de manera autònoma.

- Condicions de victòria:
- Si el jugador aconsegueix que una bola de foc impacti amb el personatge, el jugador guanya.
- Condicions de derrota:
- El rellotge arriba a 0 i el personatge continua viu.

### Personatge

- Té una sola vida. Si col·lideix amb una bola de foc, el joc acaba.
- Animacions:
- Idle: Animació quan no hi ha interacció.
- Walk: Animació de moviment al rebre interacció
- Jump: (Bonus) Animació quan el personatge salta.
- Lose: Animació de derrota que s'activa amb l'impacte amb una bola.
- Controls:
- Moviment Lateral: Fletxes direccionals o "A"/"D"
- Salt: Barra espaiadora.
El salt manté la direcció horitzontal que portava.
No hi ha doble salt ni triple salt.
Durant el salt, la bassa espaiadora no tindrà efecte.
No es pot saltar si el personatge no toca el terra.
- Moviment autònom:
- El moviment ha de ser horitzontal.
- Al arribar a la vora del terra ha de girar i continuar. S'ha de fer servir Ray i derivats.
- Velocitat continua
- Esquivar boles que van per davant: (Bonus) realitzar un mode en què detecti les boles que cauran davant del personatge i en aquell moment gira cua.

### Bola de foc

-Cicle de Vida:
- Es genera aleatòriament en una posició X a una certa alçada del terra.
- Cau en línia recta (moviment vertical) cap a terra.
- En impactar esclata i desapareix. Pot impactar contra el terra o contra el personatge.
- Força d'explosió: (Bonus) Si impacta a prop del personatge emet una força de repulsió sobre el personatge en direcció oposada al punt d'explosió.
-Animacions:
- Idle/Caiguda: Animació mentre la bola de foc cua.
- Impacte: Animació quan la bola de foc col·lideix.
- Efecte de destrucció: (Bonus) Animació de com la bola es destrueix.
- Moment d'impacte: (Bonus) La bola de foc té la capacitat d'eliminar al personatge mentre no hagi col·lidit amb el terra.
Un cop col·lideix amb el terra i durant l'animació d'explosió no eliminarà al personatge.
- Control de l'aleatorietat: Crear l'objecte que gestions la generació aleatòria de boles de foc.
Haurà de tenir en compte la freqüència de boles de foc i la posició on es genera cadascuna.

- Creació de la bola de foc:
- L'usuari generarà una bola de foc amb el botó esquerre del ratolí.
- La bola es crearà a la posició en X (horitzontal) on s'ha fet clic.
- L'alçada respecte a terra on es generen les boles serà sempre la mateixa.
- Cooldown que faci que després de fer un clic: (Bonus) Fer que un altre no aparegui una bola de foc fins que no passi un cert temps.

### Xampinyó verinós: (Bonus)

- Aleatòriament, genera un xampinyó verinós en comptes de generar-se una bola de foc.
Aquesta aleatorietat ha de ser molt baixa.

- Dels clics fets per l'usuari aleatòriament es generarà un xampinyó.

- Cicle de vida:
- Es genera aleatòriament en X i a la mateixa alçada que la bola de foc.
- Cau en línia recta (moviment vertical) cap a terra.
- A l'impactar a terra esclafirà allà durant un valor 't' de temps. Quan acaba aquest temps, si no ha impactat amb la guerrera, desapareix (destroy).
- En impactar o tocar el personatge l'emmetzina activant l'habilitat gegantina d'aquesta. Fent més difícil esquivar les boles de foc.

- Animacions:
- Idle / Caiguda: Mentre cau.
- Parpelleig: (Bonus) Animació de parpelleig abans de desaparèixer.

- En impactar amb el personatge el bolet desapareix.
- Efecte de destrucció: (Bonus) Animació de com es destrueix el xampinyó

Es tindrà en compte l'ús correcte de Prefabs, estratègies, definició de variables i funcions específiques explicades a classe.

### Pantalles:

#### StartScreen:

Pantalla amb una UI amb:
- Títol del joc (a la vostra elecció)
- Input text: per escriure el nom del personatge
- Botons de modes de joc: dos botons on escollir quin mode de joc es juga.
- Altres elements que sumin al vostre disseny.

#### HUD

UI del joc

- Comptador de temps enrere
- Altres elements que sumin al vostre joc:
- Game Over Screen: (Fi de partida)
- Títol anunciant victòria o derrota amb el nom que l'usuari ha donat al personatge.
- Botó per a tornar a compensar la partida.
- Botó per anar a la StartScreen.
- Altres elements que sumin al vostre disseny
- (Bonus) Nombre de boles de foc generades durant la partida.

#### GameManager

Creació de mínim un sol mànager que gestiona la partida. Amb funcions i variables que consulten o fan servir la resta de l'aplicació. La creació i ús de més mànagers diferents ha d'estar justificada.

