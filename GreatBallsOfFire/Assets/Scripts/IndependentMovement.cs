using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndependentMovement : MonoBehaviour
{
    private RaycastHit2D _raycastHit;

    private int _sign;
    private float _velocityX;

    private PlayerData _pData;
    private MovementData _mData;
    private Rigidbody2D _rigidBody;
    // Start is called before the first frame update
    void Start()
    {
        _sign = 1;

        _rigidBody = this.GetComponent<Rigidbody2D>();
        _pData = GetComponent<PlayerData>();
        _mData = GetComponent<MovementData>();
    }

    // Update is called once per frame
    void Update()
    {
        _pData.SetAnimations(Mathf.Abs(_rigidBody.velocity.x), false);
        _velocityX = (_sign * Time.deltaTime);
        _rigidBody.velocity += _mData.GetAcceleration() * new Vector2(_velocityX, 0);
        if (_rigidBody.velocity.magnitude > _mData.GetMaxVelocity())
        {
            _rigidBody.velocity = Vector3.ClampMagnitude(_rigidBody.velocity, _mData.GetMaxVelocity());
        }
        _raycastHit = Physics2D.Raycast(transform.position, new Vector2(_sign * 2, -1), 2);
        if(_raycastHit.collider == null)
        {
            _sign *= -1;
        }
        _mData.SetFacingDirection(_rigidBody.velocity.x);
    }
}
