using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManagement : MonoBehaviour
{
    [SerializeField] private GameObject _inputText;
    private GameObject _buttonMode1, _buttonMode2;
    // Start is called before the first frame update
    void Start()
    {
        _inputText = GameObject.Find("Name_Field");
        _buttonMode1 = GameObject.Find("GameMode1");
        _buttonMode2 = GameObject.Find("GameMode2");

        _buttonMode1.GetComponent<Button>().onClick.AddListener(() => CheckName(1));
        _buttonMode2.GetComponent<Button>().onClick.AddListener(() => CheckName(2));
    }
    void StartGame(string name, int mode)
    {
        GameManager.Instance.SetPlayerName(name);
        GameManager.Instance.SetGameMode(mode);
        SceneManager.LoadScene("SampleScene");

    }
    void CheckName(int mode)
    {
        string name = _inputText.GetComponent<InputField>().text;
        if (name != "")
        {
            StartGame(name,mode);
        }
    }


}

