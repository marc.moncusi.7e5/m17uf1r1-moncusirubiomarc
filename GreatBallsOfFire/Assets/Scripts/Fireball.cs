using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField]private Animator _fireballAnimator;

    private GamePlayManager _GPM;


    // Start is called before the first frame update
    void Start()
    {
        _GPM = GameObject.Find("GamePlayManager").GetComponent<GamePlayManager>();

    }
    private void Update()
    {
        if (transform.position.y < -GameManager.Instance.GetCamHeight())
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "FireBall":
                break;
            case "Player":
                _GPM.CharacterDeath();
                break;
            case "Floor":
                Destroy(GetComponent<Rigidbody2D>());
                Destroy(GetComponent<Collider2D>());
                _fireballAnimator.SetTrigger("ExplosionTrigger");
                Destroy(gameObject, _fireballAnimator.GetCurrentAnimatorStateInfo(0).length);
                break;
            default:
                break;
        }
    }
}
