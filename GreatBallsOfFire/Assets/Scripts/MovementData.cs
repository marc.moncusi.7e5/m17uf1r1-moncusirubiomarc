using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovementDirection
{
    //Up,
    //Down,
    Left,
    Right
}

public class MovementData : MonoBehaviour
{
    private MovementDirection _direction;
    private float _minVelocity, _maxVelocity;
    private float _acceleration;

    private GamePlayManager _GPM;


    // Start is called before the first frame update
    void Start()
    {
        _direction = MovementDirection.Right;
        _minVelocity = -5.0f;
        _maxVelocity = 5.0f;
        _acceleration = 10.0f;

        _GPM = GameObject.Find("GamePlayManager").GetComponent<GamePlayManager>();

    }
    private void Update()
    {
        if (transform.position.y < -GameManager.Instance.GetCamHeight())
        {
            _GPM.CharacterDeath();

        }
    }

    public float GetMinVelocity()
    {
        return _minVelocity;
    }
    public float GetMaxVelocity()
    {
        return _maxVelocity;
    }
    public float GetAcceleration()
    {
        return _acceleration;
    }

    public void SetFacingDirection(float velocity)
    {

        switch (_direction)
        {
            case MovementDirection.Left:
                if (velocity > 0)
                {
                    //transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
                    transform.localScale = new Vector3(1, 1, 1);
                    _direction = MovementDirection.Right;
                }
                break;
            case MovementDirection.Right:
                if (velocity < 0)
                {
                    //transform.rotation = Quaternion.Euler(new Vector3(0, 180, 0));
                    transform.localScale = new Vector3(-1, 1, 1);
                    _direction = MovementDirection.Left;
                }
                break;
        }
    }
}
