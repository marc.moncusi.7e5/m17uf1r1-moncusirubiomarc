using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerData : MonoBehaviour
{

    private Animator _playerAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _playerAnimator = this.GetComponent<Animator>();

    }

    public void SetAnimations(float velocity, bool jumping)
    {
        _playerAnimator.SetFloat("Moving", velocity);
        _playerAnimator.SetBool("Jumping", jumping);
    }
    public void JumpTrigger()
    {
        _playerAnimator.SetTrigger("Jump");
    }
    public void DeathTrigger()
    {
        _playerAnimator.SetTrigger("Hurt");
        Destroy(GetComponent<Rigidbody2D>());
        Destroy(GetComponent<Collider2D>());
        Destroy(gameObject, _playerAnimator.GetCurrentAnimatorStateInfo(0).length);
    }

    public void CharacterGameMode()
    {
        switch (GameManager.Instance.GetGameMode())
        {
            case 1:
                this.gameObject.GetComponent<IndependentMovement>().enabled = false;
                this.gameObject.GetComponent<DependentMovement>().enabled = true;
                break;
            case 2:
                this.gameObject.GetComponent<IndependentMovement>().enabled = true;
                this.gameObject.GetComponent<DependentMovement>().enabled = false;
                break;
            default:
                this.gameObject.GetComponent<IndependentMovement>().enabled = false;
                this.gameObject.GetComponent<DependentMovement>().enabled = true;
                break;
        }
    }
    public void DisableMovements()
    {
        this.gameObject.GetComponent<IndependentMovement>().enabled = false;
        this.gameObject.GetComponent<DependentMovement>().enabled = false;
    }
}
