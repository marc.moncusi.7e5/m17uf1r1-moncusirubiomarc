using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _fireball;
    private float _cooldown = 0.5f;
    private float _cooldownTimer;
    private Camera _cam;

    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;

    }

    // Update is called once per frame
    void Update()
    {
        _cooldownTimer += Time.deltaTime;
        if (_cooldownTimer > _cooldown)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _cooldownTimer = 0;
                Instantiate(_fireball, new Vector3(_cam.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 0)).x, transform.position.y, 0), new Quaternion(0, 0, 180, 0));
            }
        }
    }
}
