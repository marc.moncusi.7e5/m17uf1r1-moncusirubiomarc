using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GamePlayManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    // Update is called once per frame
    void Update()
    {
        var _timer = GameManager.Instance.GetTimer();
        if (GameManager.Instance.GetActive())
        {
            _timer -= Time.deltaTime;
        }

        if (_timer < 0)
        {
            CharacterSurvival();
        }
        GameManager.Instance.SetTimer(_timer);
    }

    public void StartGame()
    {
        GameObject.Find("Character").GetComponent<PlayerData>().CharacterGameMode();
        GameObject.Find("Spawner").GetComponent<Spawner>().SpawnerGameMode();
        GameManager.Instance.SetTimer(30.0f);
        GameManager.Instance.SetActive(true);
    }

    public void CharacterDeath()
    {
        GameObject.Find("Character").GetComponent<PlayerData>().DeathTrigger();
        GameObject.Find("Character").GetComponent<PlayerData>().DisableMovements();
        GameObject.Find("Spawner").GetComponent<Spawner>().DisableSpawners();

        GameManager.Instance.SetActive(false);

        var you = GameManager.Instance.GetPlayerName();
        switch (GameManager.Instance.GetGameMode())
        {
            case 1:
                GameObject.Find("Menu").GetComponent<HUBManager>().ActivatePanel(you + " lost");
                break;
            case 2:
                GameObject.Find("Menu").GetComponent<HUBManager>().ActivatePanel(you + " won");
                break;
        }
    }
    public void CharacterSurvival()
    {
        GameObject.Find("Character").GetComponent<PlayerData>().DisableMovements();
        GameObject.Find("Spawner").GetComponent<Spawner>().DisableSpawners();

        GameManager.Instance.SetActive(false);
        var you = GameManager.Instance.GetPlayerName();
        switch (GameManager.Instance.GetGameMode())

        {
            case 1:
                GameObject.Find("Menu").GetComponent<HUBManager>().ActivatePanel(you + " won");
                break;
            case 2:
                GameObject.Find("Menu").GetComponent<HUBManager>().ActivatePanel(you + " lost");
                break;
        }
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }
    public void GoBack()
    {
        SceneManager.LoadScene("UIMenu");
    }
}
