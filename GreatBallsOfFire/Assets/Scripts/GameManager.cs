using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private float _timer;
    [SerializeField] private int _gamemode;
    [SerializeField] private string _playerName = "";
    [SerializeField] private bool _isGameActive;

    private Camera _cam;
    private float _camHeight, _camWidth;


    // Start is called before the first frame update
    void Start()
    {
        _cam = Camera.main;
        _camHeight = _cam.orthographicSize;
        _camWidth = _cam.orthographicSize * _cam.aspect;

        _timer = 30.0f;

        DontDestroyOnLoad(this.gameObject);
    }



    public void SetPlayerName(string name)
    {
        _playerName = name;
    }
    public string GetPlayerName()
    {
        return _playerName;
    }

    public void SetGameMode(int mode)
    {
        _gamemode = mode;
    }
    public int GetGameMode()
    {
        return _gamemode;
    }

    public void SetTimer(float timer)
    {
        _timer = timer;
    }

    public float GetTimer()
    {
        return _timer;
    }
    public void SetActive(bool active)
    {
        _isGameActive = active;
    }
    public bool GetActive()
    {
        return _isGameActive;
    }

    public Camera GetCam()
    {
        return _cam;
    }
    public float GetCamHeight()
    {
        return _camHeight;
    }
    public float GetCamWidth()
    {
        return _camWidth;
    }

}
