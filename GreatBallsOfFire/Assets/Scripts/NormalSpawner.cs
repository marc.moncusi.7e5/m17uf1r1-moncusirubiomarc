using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _fireball;


    // Update is called once per frame
    void Update()
    {
        var number = Random.value;
        if (number < 0.01f)
        {
            var positionX = Random.Range(-GameManager.Instance.GetCamWidth(), GameManager.Instance.GetCamWidth());
            Instantiate(_fireball, new Vector3(positionX, transform.position.y, 0), new Quaternion(0, 0, 180, 0));
        }

    }
}
