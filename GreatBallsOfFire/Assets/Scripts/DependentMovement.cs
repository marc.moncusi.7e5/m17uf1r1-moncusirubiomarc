using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DependentMovement : MonoBehaviour
{
    private float _playerInput;
    private float _velocityX, _velocityY;
    private bool _jumping;

    private PlayerData _pData;
    private MovementData _mData;
    private Rigidbody2D _rigidBody;
    // Start is called before the first frame update
    void Start()
    {
        _playerInput = 0;
        _jumping = true;

        _rigidBody = this.GetComponent<Rigidbody2D>();
        _pData = GetComponent<PlayerData>();
        _mData = GetComponent<MovementData>();

    }

    // Update is called once per frame
    void Update()
    {
        _pData.SetAnimations(Mathf.Abs(_rigidBody.velocity.x), _jumping);
        _playerInput = Input.GetAxis("Horizontal");
        _velocityX = (Mathf.Abs(_playerInput) > 0 ? 1 : 0) * Mathf.Sign(_playerInput) * Time.deltaTime;
        _velocityY = Input.GetKey(KeyCode.Space) ? 1 : 0 * Time.deltaTime;
        if (!_jumping)
        {
            _rigidBody.velocity += _mData.GetAcceleration() * new Vector2(_velocityX, _velocityY);

            //if(_rigidbody.velocity.magnitude> _maxSpeed)
            //{
            //    _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
            //}

            if (_rigidBody.velocity.x > _mData.GetMaxVelocity() || _rigidBody.velocity.x < _mData.GetMinVelocity())
            {
                _rigidBody.velocity = new Vector2(Mathf.Clamp(_rigidBody.velocity.x, _mData.GetMinVelocity(), _mData.GetMaxVelocity()), _rigidBody.velocity.y);
            }

            if (_rigidBody.velocity.y > _mData.GetMaxVelocity() || _rigidBody.velocity.y < _mData.GetMinVelocity())
            {
                _rigidBody.velocity = new Vector2(_rigidBody.velocity.x, Mathf.Clamp(_rigidBody.velocity.y, _mData.GetMinVelocity(), _mData.GetMaxVelocity()));
            }
            
        }
        SetJumping();
        _mData.SetFacingDirection(_rigidBody.velocity.x);
    }
    private void SetJumping()
    {
        if (Mathf.Abs(_rigidBody.velocity.y) > 0 && !_jumping)
        {
            _jumping = true;
            _pData.JumpTrigger();
        }
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        _jumping = false;
    }
}
