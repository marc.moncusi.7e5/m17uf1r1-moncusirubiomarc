using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUBManager : MonoBehaviour
{
    private GameObject _inputText, _timer, _finalMessage;
    private GameObject _panel;
    private GameObject _buttonReplay, _buttonBack;

    private GamePlayManager _GPM;

    // Start is called before the first frame update
    void Start()
    {
        _inputText = GameObject.Find("PlayerName");
        _timer = GameObject.Find("Timer");
        _finalMessage = GameObject.Find("FinalMessage");
        _panel = GameObject.Find("WinLosePanel");
        _buttonReplay = GameObject.Find("ReplayButton");
        _buttonBack = GameObject.Find("BackMenuButton");

        _GPM = GameObject.Find("GamePlayManager").GetComponent<GamePlayManager>();

        _panel.SetActive(false);
        _finalMessage.SetActive(false);
        _buttonReplay.SetActive(false);
        _buttonBack.SetActive(false);
        _inputText.GetComponent<Text>().text = GameManager.Instance.GetPlayerName();
        _timer.GetComponent<Text>().text = GameManager.Instance.GetTimer().ToString("F2");

        _buttonReplay.GetComponent<Button>().onClick.AddListener(() => _GPM.RestartGame());
        _buttonBack.GetComponent<Button>().onClick.AddListener(() => _GPM.GoBack());
    }

    // Update is called once per frame
    void Update()
    {
        _timer.GetComponent<Text>().text = GameManager.Instance.GetTimer().ToString();
    }

    public void ActivatePanel(string message)
    {
        _panel.SetActive(true);
        _finalMessage.SetActive(true);
        _buttonReplay.SetActive(true);
        _buttonBack.SetActive(true);
        _finalMessage.GetComponent<Text>().text = message;
    }
}
