  using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public void SpawnerGameMode()
    {
        switch (GameManager.Instance.GetGameMode())
        {
            case 1:
                this.gameObject.GetComponent<PlayerSpawner>().enabled = false;
                this.gameObject.GetComponent<NormalSpawner>().enabled = true;
                break;
            case 2:
                this.gameObject.GetComponent<PlayerSpawner>().enabled = true;
                this.gameObject.GetComponent<NormalSpawner>().enabled = false;
                break;
            default:
                this.gameObject.GetComponent<PlayerSpawner>().enabled = false;
                this.gameObject.GetComponent<NormalSpawner>().enabled = true;
                break;
        }
    }
    public void DisableSpawners()
    {
        this.gameObject.GetComponent<PlayerSpawner>().enabled = false;
        this.gameObject.GetComponent<NormalSpawner>().enabled = false;
    }
}
